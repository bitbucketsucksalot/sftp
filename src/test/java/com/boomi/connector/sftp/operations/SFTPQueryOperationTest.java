package com.boomi.connector.sftp.operations;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.boomi.connector.api.AtomConfig;
import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Connector;
import com.boomi.connector.api.Expression;
import com.boomi.connector.api.OAuth2Context;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.api.Payload;
import com.boomi.connector.api.PayloadMetadata;
import com.boomi.connector.api.PrivateKeyStore;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.api.PublicKeyStore;
import com.boomi.connector.api.QueryFilter;
import com.boomi.connector.sftp.SFTPClient;
import com.boomi.connector.sftp.SFTPConnector;
import com.boomi.connector.sftp.constants.SFTPConstants;
import com.boomi.connector.sftp.exception.NoSuchFileFoundException;
import com.boomi.connector.sftp.exception.SFTPSdkException;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleBrowseContext;
import com.boomi.connector.testutil.SimpleOperationContext;
import com.boomi.connector.testutil.SimpleTrackedData;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;

@PowerMockIgnore("javax.management.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest(SFTPClient.class)
public class SFTPQueryOperationTest {

	@Test
	public void testExecuteQueryQueryRequestOperationResponse() throws Exception {
		JSch jsch = mock(JSch.class);
		Session session = mock(Session.class);
		ChannelSftp channel = mock(ChannelSftp.class);

		PowerMockito.whenNew(JSch.class).withNoArguments().thenReturn(jsch);
		when(jsch.getSession(any(String.class), any(String.class), any(Integer.class))).thenReturn(session);
		when(session.openChannel(any(String.class))).thenReturn(channel);
		Map<String, Object> opProperty = new HashMap<>();
		Map<String, Object> connProperty = new HashMap<>();
		opProperty.put(SFTPConstants.PROPERTY_INCLUDE_METADATA, Boolean.TRUE);
		opProperty.put(SFTPConstants.COUNT, 3l);
		connProperty.put(SFTPConstants.AUTHORIZATION_TYPE, "Using public Key");
		connProperty.put(SFTPConstants.IS_MAX_EXCHANGE, Boolean.FALSE);
		connProperty.put(SFTPConstants.REMOTE_DIRECTORY, "/abc");

		LsEntry entry = mock(LsEntry.class);

		Vector vect = new Vector();
		vect.add(entry);
		when(channel.ls(any(String.class))).thenReturn(vect);
		SftpATTRS attrs = mock(SftpATTRS.class);
		attrs.setSIZE(1);
		when(entry.getAttrs()).thenReturn(attrs);
		when(attrs.isLink()).thenReturn(Boolean.FALSE);
		when(entry.getFilename()).thenReturn("yweurywi");

		when(channel.lstat(any(String.class))).thenReturn(attrs);
		SFTPConnector connector = new SFTPConnector();
		SimpleOperationContext opContext = new SimpleOperationContext(null, connector, OperationType.QUERY,
				connProperty, opProperty, null, null);
		QueryOpContext qopcontext = new QueryOpContext(null, connector, OperationType.QUERY, connProperty, opProperty,
				null, null, "QUERY");

		SimpleBrowseContext context = new SimpleBrowseContext(null, connector, OperationType.QUERY, "LIST",
				connProperty, opProperty);
		ConnectorTester tester = new ConnectorTester(connector);

		Map<ObjectDefinitionRole, String> cookie = new HashMap<>();
		cookie.put(ObjectDefinitionRole.OUTPUT, "true");

		List<InputStream> streamList = new ArrayList<>();
		streamList.add(new ByteArrayInputStream("abc".getBytes("UTF-8")));

		Map<String, String> dynamicProperty = new HashMap<>();
		dynamicProperty.put(SFTPConstants.PROPERTY_FILENAME, "file1.txt");
		SimpleTrackedData trackedData = new SimpleTrackedData(1, new ByteArrayInputStream("abc".getBytes("UTF-8")),
				null, dynamicProperty);
		List<SimpleTrackedData> listTrackData = new ArrayList<>();
		listTrackData.add(trackedData);

		tester.setBrowseContext(context);
		tester.setOperationContext(qopcontext);

		QueryFilter filter = new QueryFilter();
		tester.executeQueryOperation(filter);
	}

	
	@Test
	public void testExecuteQueryQueryRequestOperationResponseInvalidQueryType() throws Exception {
		JSch jsch = mock(JSch.class);
		Session session = mock(Session.class);
		ChannelSftp channel = mock(ChannelSftp.class);

		PowerMockito.whenNew(JSch.class).withNoArguments().thenReturn(jsch);
		when(jsch.getSession(any(String.class), any(String.class), any(Integer.class))).thenReturn(session);
		when(session.openChannel(any(String.class))).thenReturn(channel);
		Map<String, Object> opProperty = new HashMap<>();
		Map<String, Object> connProperty = new HashMap<>();
		opProperty.put(SFTPConstants.PROPERTY_INCLUDE_METADATA, Boolean.TRUE);
		opProperty.put(SFTPConstants.COUNT, 3l);
		connProperty.put(SFTPConstants.AUTHORIZATION_TYPE, "Using public Key");
		connProperty.put(SFTPConstants.IS_MAX_EXCHANGE, Boolean.FALSE);
		connProperty.put(SFTPConstants.REMOTE_DIRECTORY, "/abc");

		LsEntry entry = mock(LsEntry.class);

		Vector vect = new Vector();
		vect.add(entry);
		when(channel.ls(any(String.class))).thenReturn(vect);
		SftpATTRS attrs = mock(SftpATTRS.class);
		attrs.setSIZE(1);
		when(entry.getAttrs()).thenReturn(attrs);
		when(attrs.isLink()).thenReturn(Boolean.FALSE);
		when(entry.getFilename()).thenReturn("yweurywi");

		when(channel.lstat(any(String.class))).thenReturn(attrs);
		SFTPConnector connector = new SFTPConnector();
		SimpleOperationContext opContext = new SimpleOperationContext(null, connector, OperationType.QUERY,
				connProperty, opProperty, null, null);
		QueryOpContext qopcontext = new QueryOpContext(null, connector, OperationType.QUERY, connProperty, opProperty,
				null, null, "qwieqw");

		SimpleBrowseContext context = new SimpleBrowseContext(null, connector, OperationType.QUERY, "LIST",
				connProperty, opProperty);
		ConnectorTester tester = new ConnectorTester(connector);

		Map<ObjectDefinitionRole, String> cookie = new HashMap<>();
		cookie.put(ObjectDefinitionRole.OUTPUT, "true");

		List<InputStream> streamList = new ArrayList<>();
		streamList.add(new ByteArrayInputStream("abc".getBytes("UTF-8")));

		Map<String, String> dynamicProperty = new HashMap<>();
		dynamicProperty.put(SFTPConstants.PROPERTY_FILENAME, "file1.txt");
		SimpleTrackedData trackedData = new SimpleTrackedData(1, new ByteArrayInputStream("abc".getBytes("UTF-8")),
				null, dynamicProperty);
		List<SimpleTrackedData> listTrackData = new ArrayList<>();
		listTrackData.add(trackedData);

		tester.setBrowseContext(context);
		tester.setOperationContext(qopcontext);

		QueryFilter filter = new QueryFilter();
		tester.executeQueryOperation(filter);
	}
	
	
	@Test
	public void testExecuteQueryQueryRequestOperationResponseLIST() throws Exception {
		JSch jsch = mock(JSch.class);
		Session session = mock(Session.class);
		ChannelSftp channel = mock(ChannelSftp.class);

		PowerMockito.whenNew(JSch.class).withNoArguments().thenReturn(jsch);
		when(jsch.getSession(any(String.class), any(String.class), any(Integer.class))).thenReturn(session);
		when(session.openChannel(any(String.class))).thenReturn(channel);
		Map<String, Object> opProperty = new HashMap<>();
		Map<String, Object> connProperty = new HashMap<>();
		opProperty.put(SFTPConstants.PROPERTY_INCLUDE_METADATA, Boolean.TRUE);
		opProperty.put(SFTPConstants.COUNT, 3l);
		connProperty.put(SFTPConstants.AUTHORIZATION_TYPE, "Using public Key");
		connProperty.put(SFTPConstants.IS_MAX_EXCHANGE, Boolean.FALSE);
		connProperty.put(SFTPConstants.REMOTE_DIRECTORY, "/abc");

		LsEntry entry = mock(LsEntry.class);

		NoSuchFileFoundException ex=new NoSuchFileFoundException("abc");
		SFTPSdkException exsdk=new SFTPSdkException("error", new Throwable());
		Vector vect = new Vector();
		vect.add(entry);
		when(channel.ls(any(String.class))).thenReturn(vect);
		SftpATTRS attrs = mock(SftpATTRS.class);
		attrs.setSIZE(1);
		when(entry.getAttrs()).thenReturn(attrs);
		when(attrs.isLink()).thenReturn(Boolean.FALSE);
		when(entry.getFilename()).thenReturn("yweurywi");

		when(channel.lstat(any(String.class))).thenReturn(attrs);
		SFTPConnector connector = new SFTPConnector();
		SimpleOperationContext opContext = new SimpleOperationContext(null, connector, OperationType.QUERY,
				connProperty, opProperty, null, null);
		QueryOpContext qopcontext = new QueryOpContext(null, connector, OperationType.QUERY, connProperty, opProperty,
				null, null, "LIST");

		SimpleBrowseContext context = new SimpleBrowseContext(null, connector, OperationType.QUERY, "LIST",
				connProperty, opProperty);
		ConnectorTester tester = new ConnectorTester(connector);

		Map<ObjectDefinitionRole, String> cookie = new HashMap<>();
		cookie.put(ObjectDefinitionRole.OUTPUT, "true");

		List<InputStream> streamList = new ArrayList<>();
		streamList.add(new ByteArrayInputStream("abc".getBytes("UTF-8")));

		Map<String, String> dynamicProperty = new HashMap<>();
		dynamicProperty.put(SFTPConstants.PROPERTY_FILENAME, "file1.txt");
		SimpleTrackedData trackedData = new SimpleTrackedData(1, new ByteArrayInputStream("abc".getBytes("UTF-8")),
				null, dynamicProperty);
		List<SimpleTrackedData> listTrackData = new ArrayList<>();
		listTrackData.add(trackedData);

		tester.setBrowseContext(context);
		tester.setOperationContext(qopcontext);

		QueryFilter filter = new QueryFilter();
		tester.executeQueryOperation(filter);
	}
	
	@Test
	public void testExecuteQueryQueryRequestOperationResponseQUERY() throws Exception {
		JSch jsch = mock(JSch.class);
		Session session = mock(Session.class);
		ChannelSftp channel = mock(ChannelSftp.class);

		PowerMockito.whenNew(JSch.class).withNoArguments().thenReturn(jsch);
		when(jsch.getSession(any(String.class), any(String.class), any(Integer.class))).thenReturn(session);
		when(session.openChannel(any(String.class))).thenReturn(channel);
		Map<String, Object> opProperty = new HashMap<>();
		Map<String, Object> connProperty = new HashMap<>();
		opProperty.put(SFTPConstants.PROPERTY_INCLUDE_METADATA, Boolean.TRUE);
		opProperty.put(SFTPConstants.COUNT, 3l);
		connProperty.put(SFTPConstants.AUTHORIZATION_TYPE, "Using public Key");
		connProperty.put(SFTPConstants.IS_MAX_EXCHANGE, Boolean.FALSE);
		connProperty.put(SFTPConstants.REMOTE_DIRECTORY, "/abc");

		LsEntry entry = mock(LsEntry.class);

		NoSuchFileFoundException ex=new NoSuchFileFoundException("abc");
		SFTPSdkException exsdk=new SFTPSdkException("error", new Throwable());
		Vector vect = new Vector();
		vect.add(entry);
		when(channel.ls(any(String.class))).thenReturn(vect);
		SftpATTRS attrs = mock(SftpATTRS.class);
		attrs.setSIZE(1);
		when(entry.getAttrs()).thenReturn(attrs);
		when(attrs.isLink()).thenReturn(Boolean.FALSE);
		when(entry.getFilename()).thenReturn("yweurywi");

		when(channel.lstat(any(String.class))).thenReturn(attrs);
		SFTPConnector connector = new SFTPConnector();
		SimpleOperationContext opContext = new SimpleOperationContext(null, connector, OperationType.QUERY,
				connProperty, opProperty, null, null);
		QueryOpContext qopcontext = new QueryOpContext(null, connector, OperationType.QUERY, connProperty, opProperty,
				null, null, "LIST");

		SimpleBrowseContext context = new SimpleBrowseContext(null, connector, OperationType.QUERY, "LIST",
				connProperty, opProperty);
		ConnectorTester tester = new ConnectorTester(connector);

		Map<ObjectDefinitionRole, String> cookie = new HashMap<>();
		cookie.put(ObjectDefinitionRole.OUTPUT, "true");

		List<InputStream> streamList = new ArrayList<>();
		streamList.add(new ByteArrayInputStream("abc".getBytes("UTF-8")));

		Map<String, String> dynamicProperty = new HashMap<>();
		dynamicProperty.put(SFTPConstants.PROPERTY_FILENAME, "file1.txt");
		SimpleTrackedData trackedData = new SimpleTrackedData(1, new ByteArrayInputStream("abc".getBytes("UTF-8")),
				null, dynamicProperty);
		List<SimpleTrackedData> listTrackData = new ArrayList<>();
		listTrackData.add(trackedData);

		tester.setBrowseContext(context);
		tester.setOperationContext(qopcontext);

		QueryFilter filter = new QueryFilter();
		
		//filter.setExpression(value);
		tester.executeQueryOperation(filter);
	}
}

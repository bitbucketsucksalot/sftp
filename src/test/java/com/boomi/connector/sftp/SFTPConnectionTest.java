package com.boomi.connector.sftp;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.FilterData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.api.TrackedData;
import com.boomi.connector.sftp.constants.SFTPConstants;
import com.boomi.connector.sftp.results.MultiResult;
import com.boomi.connector.testutil.SimpleBrowseContext;
import com.boomi.connector.testutil.SimpleTrackedData;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

@PowerMockIgnore("javax.management.*")
@RunWith(PowerMockRunner.class)
@PrepareForTest(SFTPClient.class)
public class SFTPConnectionTest {

	
	//  @Rule public PowerMockRule rule = new PowerMockRule();
	 

	@Test
	public void testGetClient() throws Exception {

		JSch jsch = mock(JSch.class);
		Session session = mock(Session.class);
		ChannelSftp channel = mock(ChannelSftp.class);
		PowerMockito.whenNew(JSch.class).withNoArguments().thenReturn(jsch);
		when(jsch.getSession(any(String.class), any(String.class), any(Integer.class))).thenReturn(session);
		when(session.openChannel(any(String.class))).thenReturn(channel);
		Map<String, Object> opProperty = new HashMap<>();
		Map<String, Object> connProperty = new HashMap<>();
		opProperty.put(SFTPConstants.PROPERTY_INCLUDE_METADATA, Boolean.TRUE);
		connProperty.put(SFTPConstants.AUTHORIZATION_TYPE, "Using public Key");
		connProperty.put(SFTPConstants.IS_MAX_EXCHANGE, Boolean.FALSE);
		SimpleBrowseContext context = new SimpleBrowseContext(null, null, OperationType.CREATE, connProperty,
				opProperty);
		SFTPConnection conn = new SFTPConnection(context);
		conn.openConnection();
        conn.getClient();
	}

	@Test
	public void testGetClientUserNamePassword() throws Exception {

		JSch jsch = mock(JSch.class);
		Session session = mock(Session.class);
		ChannelSftp channel = mock(ChannelSftp.class);
		PowerMockito.whenNew(JSch.class).withNoArguments().thenReturn(jsch);
		when(jsch.getSession(any(String.class), any(String.class), any(Integer.class))).thenReturn(session);
		when(session.openChannel(any(String.class))).thenReturn(channel);
		Map<String, Object> opProperty = new HashMap<>();
		Map<String, Object> connProperty = new HashMap<>();
		opProperty.put(SFTPConstants.PROPERTY_INCLUDE_METADATA, Boolean.TRUE);
		connProperty.put(SFTPConstants.AUTHORIZATION_TYPE, "Username and Password");
		connProperty.put(SFTPConstants.IS_MAX_EXCHANGE, Boolean.FALSE);
		SimpleBrowseContext context = new SimpleBrowseContext(null, null, OperationType.CREATE, connProperty,
				opProperty);
		SFTPConnection conn = new SFTPConnection(context);
		conn.openConnection();
        conn.getClient();
	}
	
	@Test
	public void testGetPathsHandler() throws Exception {
		JSch jsch = mock(JSch.class);
		Session session = mock(Session.class);
		ChannelSftp channel = mock(ChannelSftp.class);
		PowerMockito.whenNew(JSch.class).withNoArguments().thenReturn(jsch);
		when(jsch.getSession(any(String.class), any(String.class), any(Integer.class))).thenReturn(session);
		when(session.openChannel(any(String.class))).thenReturn(channel);
		Map<String, Object> opProperty = new HashMap<>();
		Map<String, Object> connProperty = new HashMap<>();
		opProperty.put(SFTPConstants.PROPERTY_INCLUDE_METADATA, Boolean.TRUE);
		connProperty.put(SFTPConstants.AUTHORIZATION_TYPE, "Using public Key");
		connProperty.put(SFTPConstants.IS_MAX_EXCHANGE, Boolean.FALSE);
		SimpleBrowseContext context = new SimpleBrowseContext(null, null, OperationType.CREATE, connProperty,
				opProperty);
		SFTPConnection conn = new SFTPConnection(context);
		conn.openConnection();
		conn.getPathsHandler();
	}



	@Test
	public void testIsConnected() throws Exception {
		JSch jsch = mock(JSch.class);
		Session session = mock(Session.class);
		ChannelSftp channel = mock(ChannelSftp.class);
		PowerMockito.whenNew(JSch.class).withNoArguments().thenReturn(jsch);
		when(jsch.getSession(any(String.class), any(String.class), any(Integer.class))).thenReturn(session);
		when(session.openChannel(any(String.class))).thenReturn(channel);
		Map<String, Object> opProperty = new HashMap<>();
		Map<String, Object> connProperty = new HashMap<>();
		opProperty.put(SFTPConstants.PROPERTY_INCLUDE_METADATA, Boolean.TRUE);
		connProperty.put(SFTPConstants.AUTHORIZATION_TYPE, "Using public Key");
		connProperty.put(SFTPConstants.IS_MAX_EXCHANGE, Boolean.FALSE);
		SimpleBrowseContext context = new SimpleBrowseContext(null, null, OperationType.CREATE, connProperty,
				opProperty);
		SFTPConnection conn = new SFTPConnection(context);
		conn.openConnection();
		conn.isConnected();
	}

	@Test
	public void testCloseConnection() throws Exception {
		JSch jsch = mock(JSch.class);
		Session session = mock(Session.class);
		ChannelSftp channel = mock(ChannelSftp.class);
		PowerMockito.whenNew(JSch.class).withNoArguments().thenReturn(jsch);
		when(jsch.getSession(any(String.class), any(String.class), any(Integer.class))).thenReturn(session);
		when(session.openChannel(any(String.class))).thenReturn(channel);
		Map<String, Object> opProperty = new HashMap<>();
		Map<String, Object> connProperty = new HashMap<>();
		opProperty.put(SFTPConstants.PROPERTY_INCLUDE_METADATA, Boolean.TRUE);
		connProperty.put(SFTPConstants.AUTHORIZATION_TYPE, "Using public Key");
		connProperty.put(SFTPConstants.IS_MAX_EXCHANGE, Boolean.FALSE);
		SimpleBrowseContext context = new SimpleBrowseContext(null, null, OperationType.CREATE, connProperty,
				opProperty);
		SFTPConnection conn = new SFTPConnection(context);
		conn.openConnection();
		conn.closeConnection();
	}

	@Test
	public void testPutFileInputStreamString() throws Exception {
		JSch jsch = mock(JSch.class);
		Session session = mock(Session.class);
		ChannelSftp channel = mock(ChannelSftp.class);
		PowerMockito.whenNew(JSch.class).withNoArguments().thenReturn(jsch);
		when(jsch.getSession(any(String.class), any(String.class), any(Integer.class))).thenReturn(session);
		when(session.openChannel(any(String.class))).thenReturn(channel);
		doNothing().when(channel).put(any(InputStream.class), any(String.class));
		Map<String, Object> opProperty = new HashMap<>();
		Map<String, Object> connProperty = new HashMap<>();
		opProperty.put(SFTPConstants.PROPERTY_INCLUDE_METADATA, Boolean.TRUE);
		connProperty.put(SFTPConstants.AUTHORIZATION_TYPE, "Using public Key");
		connProperty.put(SFTPConstants.IS_MAX_EXCHANGE, Boolean.FALSE);
		SimpleBrowseContext context = new SimpleBrowseContext(null, null, OperationType.CREATE, connProperty,
				opProperty);
		SFTPConnection conn = new SFTPConnection(context);
		conn.openConnection();
		conn.putFile( new ByteArrayInputStream( "abc".getBytes( "UTF-8" ) ), "abc");
		
	}

	

	@Test
	public void testRenameFile() throws Exception {
		JSch jsch = mock(JSch.class);
		Session session = mock(Session.class);
		ChannelSftp channel = mock(ChannelSftp.class);
		PowerMockito.whenNew(JSch.class).withNoArguments().thenReturn(jsch);
		when(jsch.getSession(any(String.class), any(String.class), any(Integer.class))).thenReturn(session);
		when(session.openChannel(any(String.class))).thenReturn(channel);
		doNothing().when(channel).put(any(InputStream.class), any(String.class));
		Map<String, Object> opProperty = new HashMap<>();
		Map<String, Object> connProperty = new HashMap<>();
		opProperty.put(SFTPConstants.PROPERTY_INCLUDE_METADATA, Boolean.TRUE);
		connProperty.put(SFTPConstants.AUTHORIZATION_TYPE, "Using public Key");
		connProperty.put(SFTPConstants.IS_MAX_EXCHANGE, Boolean.FALSE);
		SimpleBrowseContext context = new SimpleBrowseContext(null, null, OperationType.CREATE, connProperty,
				opProperty);
		SFTPConnection conn = new SFTPConnection(context);
		conn.openConnection();
	}


	@Test(expected=ConnectorException.class)
	public void testQueryDirectory() throws Exception {
		JSch jsch = mock(JSch.class);
		Session session = mock(Session.class);
		ChannelSftp channel = mock(ChannelSftp.class);
		PowerMockito.whenNew(JSch.class).withNoArguments().thenReturn(jsch);
		when(jsch.getSession(any(String.class), any(String.class), any(Integer.class))).thenReturn(session);
		when(session.openChannel(any(String.class))).thenReturn(channel);
		doNothing().when(channel).put(any(InputStream.class), any(String.class));
		Map<String, Object> opProperty = new HashMap<>();
		Map<String, Object> connProperty = new HashMap<>();
		opProperty.put(SFTPConstants.PROPERTY_INCLUDE_METADATA, Boolean.TRUE);
		connProperty.put(SFTPConstants.AUTHORIZATION_TYPE, "Using public Key");
		connProperty.put(SFTPConstants.IS_MAX_EXCHANGE, Boolean.FALSE);
		connProperty.put(SFTPConstants.PROPERTY_REMOTE_DIRECTORY, "abcdef");
		SimpleBrowseContext context = new SimpleBrowseContext(null, null, OperationType.CREATE, connProperty,
				opProperty);
		SFTPConnection conn = new SFTPConnection(context);
		conn.openConnection();
		FilterData input = mock(FilterData.class);
		
		OperationResponse operationResponse = mock(OperationResponse.class);
		MultiResult multiResult = new MultiResult(operationResponse , input);
		conn.queryDirectory(multiResult);
	}


}

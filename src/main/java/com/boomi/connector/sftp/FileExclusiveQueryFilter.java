/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp;

import com.boomi.connector.api.FilterData;
import com.boomi.connector.sftp.actions.RetryableGetFileMetadataAction;
import com.boomi.connector.sftp.common.FileMetadata;

import java.io.File;

import java.nio.file.Path;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class FileExclusiveQueryFilter extends FileQueryFilter {
	public FileExclusiveQueryFilter(File directory, FilterData input,
			RetryableGetFileMetadataAction retryableFileMetadata) {
		super(directory, input, retryableFileMetadata);
	}

	@Override
	public boolean accept(Path entry) {
		return this.accept(new FileMetadata(entry, super.retryableFilemetadataAction));
	}

	@Override
	public boolean accept(FileMetadata meta) {
		return meta.isRegularFile() && super.accept(meta);
	}
}

/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp.retry;

import com.boomi.util.retry.NeverRetry;
import com.boomi.util.retry.RetryStrategy;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public abstract class RetryStrategyFactory {
	private static final RetryStrategy ALWAYS_RETRY_STRATEGY = new RetryStrategy() {

		public boolean shouldRetry(int retryNumber, Object status) {
			return true;
		}

		public void backoff(int retryNumber) {
			throw new UnsupportedOperationException();
		}
	};

	public abstract RetryStrategy createRetryStrategy();

	public static RetryStrategyFactory createFactory(int maxRetries) {
		switch (maxRetries) {
		case -1: {
			return new AlwaysRetryStrategyFactory();
		}
		case 0: {
			return new NeverRetryStrategyFactory();
		}
		}
		return new LimitedRetryStrategyFactory(maxRetries);
	}

	static final class NeverRetryStrategyFactory extends RetryStrategyFactory {
		NeverRetryStrategyFactory() {
		}

		@Override
		public RetryStrategy createRetryStrategy() {
			return NeverRetry.INSTANCE;
		}
	}

	static final class AlwaysRetryStrategyFactory extends RetryStrategyFactory {
		AlwaysRetryStrategyFactory() {
		}

		@Override
		public RetryStrategy createRetryStrategy() {
			return ALWAYS_RETRY_STRATEGY;
		}
	}

}

/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp.exception;

import com.jcraft.jsch.SftpException;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class SFTPSdkException extends RuntimeException {

	private static final long serialVersionUID = -2183465319487851521L;

	public SFTPSdkException(String statusMessage) {
		super(statusMessage);

	}

	public SFTPSdkException(String string, SftpException e) {
		super(string, e);
	}

	public SFTPSdkException(String errorMessage, Throwable err) {
		super(errorMessage, err);
	}

}

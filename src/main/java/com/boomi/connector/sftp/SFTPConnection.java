/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.boomi.connector.api.TrackedData;
import com.boomi.connector.sftp.actions.RetryableAction;
import com.boomi.connector.sftp.actions.RetryableGetFileListInDirectory;
import com.boomi.connector.sftp.actions.RetryableGetFileMetadataAction;
import com.boomi.connector.sftp.actions.RetryableRetrieveFileAction;
import com.boomi.connector.sftp.common.ExtendedSFTPFileMetadata;
import com.boomi.connector.sftp.common.FileMetadata;
import com.boomi.connector.sftp.common.MeteredTempOutputStream;
import com.boomi.connector.sftp.common.PathsHandler;
import com.boomi.connector.sftp.common.PropertiesUtil;
import com.boomi.connector.sftp.common.SFTPFileMetadata;
import com.boomi.connector.sftp.common.UnixPathsHandler;
import com.boomi.connector.sftp.constants.SFTPConstants;
import com.boomi.connector.sftp.results.BaseResult;
import com.boomi.connector.sftp.results.EmptySuccess;
import com.boomi.connector.sftp.results.ErrorResult;
import com.boomi.connector.sftp.results.MultiResult;
import com.boomi.connector.sftp.results.Result;
import com.boomi.connector.sftp.retry.RetryStrategyFactory;
import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.FilterData;
import com.boomi.connector.api.JsonPayloadUtil;
import com.boomi.connector.api.ObjectIdData;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.Payload;
import com.boomi.connector.api.PayloadUtil;
import com.boomi.connector.util.BaseConnection;
import com.boomi.util.IOUtil;
import com.boomi.util.LogUtil;
import com.boomi.util.StreamUtil;
import com.boomi.util.StringUtil;
import com.boomi.util.json.JSONUtil;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.SftpATTRS;

/**
 * @author Omesh Deoli
 *
 *         ${tags}
 */
public class SFTPConnection extends BaseConnection {

	private final PathsHandler pathsHandler;

	private SFTPClient client;

	public SFTPClient getClient() {
		return client;
	}

	private final String remoteDirectory;

	private InputStream fileContent;

	public PathsHandler getPathsHandler() {
		return pathsHandler;
	}

	public SFTPConnection(BrowseContext context) {
		super(context);
		this.pathsHandler = new UnixPathsHandler();
		this.remoteDirectory = StringUtil
				.trim(context.getConnectionProperties().getProperty(SFTPConstants.REMOTE_DIRECTORY));

	}

	public void openConnection() {
		this.client = createSftpClient(new PropertiesUtil(this.getContext()));
		this.client.openConnection();
	}

	private SFTPClient createSftpClient(PropertiesUtil propertiesUtil) {

		if (propertiesUtil.getAuthType().equals(AuthType.PUBLIC_KEY.getAuthType())) {
			PublicKeyParam param;
			if (!propertiesUtil.isUseKeyContentEnabled()) {
				param = new PublicKeyParam(propertiesUtil.getpassphrase(), propertiesUtil.getprvtkeyPath(),
						propertiesUtil.getUsername(), propertiesUtil.isUseKeyContentEnabled());
			} else {
				param = new PublicKeyParam(propertiesUtil.getUsername(),propertiesUtil.getpassphrase(), propertiesUtil.getPrivateKeyContent(),
						propertiesUtil.getPublicKeyContent(), propertiesUtil.getKeyPairName(),
						propertiesUtil.isUseKeyContentEnabled());
			}
			return new SFTPClient(propertiesUtil.getHostname(), propertiesUtil.getPort(), AuthType.PUBLIC_KEY, param,propertiesUtil);
		} else {
			PasswordParam passwordParam = new PasswordParam(propertiesUtil.getUsername(), propertiesUtil.getPassword());
			return new SFTPClient(propertiesUtil.getHostname(), propertiesUtil.getPort(), AuthType.PASSWORD,
					passwordParam, propertiesUtil);
		}
	}

	public boolean isConnected() {
		return this.client.isConnected();
	}

	public void closeConnection() {
		client.closeConnection();

	}

	public void putFile(InputStream content, String filePath) {

		client.putFile(content, filePath);

	}

	public String getDocOrOperationProperty(TrackedData input, String propName) {
		String propertyValue = SFTPUtil.getDocProperty(input, propName);
		if (StringUtil.isNotBlank((String) propertyValue)) {
			return propertyValue;
		}
		return StringUtil.trim((String) this.getOperationContext().getOperationProperties().getProperty(propName));
	}

	public void renameFile(String fullPath, String newFullPath) {

		client.renameFile(fullPath, newFullPath);

	}

	public void putFile(InputStream content, String filePath, int mode) {

		client.putFile(content, filePath, mode);

	}

	public boolean isFilePresent(String filePath) {

		return client.isFilePresent(filePath);
	}

	public void deleteFile(String filePath) {

		client.deleteFile(filePath);

	}

	public Result deleteFile(ObjectIdData input) {
		String fileFullPath = getFileFullPath(input);
		deleteFile(fileFullPath);
		return new EmptySuccess();
	}

	private String getFileFullPath(ObjectIdData input) {

		String enteredDirectory = getEnteredRemoteDirectory(input);
		String homeOrRemoteDir = pathsHandler.resolvePaths(client.getHomeDirectory(), enteredDirectory);
		String enteredFilePath = input.getObjectId();
		if (StringUtil.isBlank((String) enteredFilePath)) {
			throw new ConnectorException(SFTPConstants.ERROR_MISSING_INPUT_FILENAME);
		}
		return pathsHandler.joinPaths(homeOrRemoteDir, enteredFilePath);

	}

	public InputStream getFileStream(String fullFilePath) {

		return client.getFileStream(fullFilePath);

	}

	public void closeStream(InputStream stream) {
		IOUtil.closeQuietly((Closeable[]) new Closeable[] { stream });
	}

	public void createDirectory(String filePath) {
		client.createDirectory(filePath);

	}

	public void createNestedDirectories(String fullPath) {
		client.createNestedDirectory(fullPath);

	}

	public String getCurrentDirectory() {

		return client.getCurrentDirectory();

	}

	public String getEnteredRemoteDirectory(TrackedData input) {

		String dirPath = SFTPUtil.getDocProperty(input, SFTPConstants.REMOTE_DIRECTORY);
		return StringUtil.defaultIfBlank((String) dirPath, (String) this.remoteDirectory);
	}

	public void changeCurrentDirectory(String remoteDir) {
		client.changeCurrentDirectory(remoteDir);

	}

	public String findUniqueFileName(String enteredFileName, String remoteDir) {
		List<String> fileListing = client.listNames(remoteDir);
		return client.verifyUniqueFileName(enteredFileName, fileListing);

	}

	public boolean fileExists(String enteredFileName, String remoteDir) {

		return client.isFilePresent(pathsHandler.joinPaths(remoteDir, enteredFileName));
	}

	public boolean fileExists(String fullPath) {

		return client.isFilePresent(fullPath);
	}

	public ExtendedSFTPFileMetadata getFileMetadata(String dirFullPath, String fileName) {

		RetryableGetFileMetadataAction retryableMetadataaction = new RetryableGetFileMetadataAction(this, dirFullPath,
				fileName);
		retryableMetadataaction.execute();
		String formattedDate = SFTPUtil
				.formatDate(SFTPUtil.parseDate(retryableMetadataaction.get_fileMetaData().getMTime()));

		return new ExtendedSFTPFileMetadata(dirFullPath, fileName, formattedDate);
	}

	public void queryDirectory(MultiResult input) {
		this.doQuery(input, true, new ResultBuilder() {

			@Override
			public BaseResult makeResult(FileMetadata meta, RetryableAction retryableGetaction) throws IOException {
				try {
					retryableGetaction.execute();
					fileContent = ((RetryableRetrieveFileAction) retryableGetaction).get_outputStream().toInputStream();
					return new BaseResult(PayloadUtil.toPayload(fileContent));

				} finally {
					((RetryableRetrieveFileAction) retryableGetaction).close();
				}

			}

		});

	}

	private void doQuery(MultiResult result, boolean filesOnly, ResultBuilder resultBuilder) {
		FilterData input = result.getInput();

		String dirFullPath = getRemoteDirFullPath((TrackedData) input);

		if (dirFullPath == null) {
			throw new ConnectorException(SFTPConstants.ERROR_MISSING_DIRECTORY);
		}
		Logger logger = input.getLogger();
		File directory = new File(dirFullPath);
		RetryableGetFileMetadataAction retryableFileMetadataForDirectory = new RetryableGetFileMetadataAction(this,
				dirFullPath, (TrackedData) input);
		FileQueryFilter filter = this.makeFilter(input, filesOnly, directory, retryableFileMetadataForDirectory);

		try {

			long limit = this.getLimit();
			RetryableGetFileListInDirectory retriableGetFileList = new RetryableGetFileListInDirectory(this,
					dirFullPath, input, RetryStrategyFactory.createFactory(6));
			retriableGetFileList.execute();

			List<LsEntry> fileList = retriableGetFileList.get_fileList();

			for (LsEntry entry : fileList) {
				try {
					String fullFilePath = pathsHandler.joinPaths(dirFullPath, entry.getFilename());
					File fullFilePathObj = new File(fullFilePath);
					RetryableGetFileMetadataAction retryableFileMetadataForEachFile = new RetryableGetFileMetadataAction(
							this, dirFullPath, (TrackedData) input);
					BaseResult part = getPartialResult(filter, logger, fullFilePathObj.toPath(), resultBuilder,
							retryableFileMetadataForEachFile);
					if (part != null) {
						result.addPartialResult(fullFilePath, part);
					}
					if ((long) result.getSize() == limit) {
						break;
					}
				} finally {
					closeStream(fileContent);
				}
			}
		} catch (Exception e) {
			LogUtil.warning((Logger) logger, SFTPConstants.ERROR_QUERYING_FILES, e);
			OperationStatus status = result.isEmpty() ? OperationStatus.FAILURE : OperationStatus.APPLICATION_ERROR;
			result.addPartialResult(new ErrorResult(status, e));

		}

	}

	private String getRemoteDirFullPath(TrackedData input) {
		String enteredDirectory = getEnteredRemoteDirectory(input);
		return pathsHandler.resolvePaths(client.getHomeDirectory(), enteredDirectory);

	}

	private FileQueryFilter makeFilter(FilterData input, boolean filesOnly, File dirFullPath,
			RetryableGetFileMetadataAction retryableFileMetadata) {
		if (filesOnly) {
			return new FileExclusiveQueryFilter(dirFullPath, input, retryableFileMetadata);
		}
		return new FileQueryFilter(dirFullPath, input, retryableFileMetadata);
	}

	private BaseResult getPartialResult(FileQueryFilter filter, Logger logger, Path path, ResultBuilder resultBuilder,
			RetryableGetFileMetadataAction retryableFileMetadata) {
		RetryableRetrieveFileAction retryableGetfileAction = null;
		try {
			FileMetadata meta = new FileMetadata(path, retryableFileMetadata);
			if (filter.accept(meta)) {
				if (logger.isLoggable(Level.FINE)) {
					LogUtil.fine((Logger) logger, "Adding %s", (Object[]) new Object[] { path.toString() });
				}
				retryableGetfileAction = new RetryableRetrieveFileAction(this, meta.getParent(),
						FileMetadata.joinPaths(meta.getParent(), meta.getFileName()), filter.getInput(),
						RetryStrategyFactory.createFactory(12));
				return resultBuilder.makeResult(meta, retryableGetfileAction);
				
			}
			return null;
		} catch (Exception e) {
			LogUtil.warning((Logger) logger, e, SFTPConstants.UNEXPECTED_ERROR_OCCURED,
					(Object[]) new Object[] { path });
			return new ErrorResult(OperationStatus.APPLICATION_ERROR, e);
		} finally {
			if (retryableGetfileAction != null) {
				retryableGetfileAction.close();
			}
		}
	}

	private static interface ResultBuilder {
		public BaseResult makeResult(FileMetadata var2, RetryableAction action) throws IOException;
	}

	private long getLimit() {
		long limit = this.getContext().getOperationProperties().getLongProperty(SFTPConstants.COUNT);
		if (limit == -1L) {
			return Long.MAX_VALUE;
		}
		if (limit <= 0L) {
			throw new IllegalArgumentException(String.format(SFTPConstants.LIMIT_MUST_BE_POSITIVE, limit));
		}
		return limit;
	}

	public void listDirectory(MultiResult input) {
		this.doQuery(input, false, new ResultBuilder() {

			@Override
			public BaseResult makeResult(FileMetadata meta, RetryableAction action) {
				return new BaseResult(SFTPConnection.makeJsonPayload(meta));
			}
		});

	}

	private static Payload makeJsonPayload(FileMetadata meta) {
		return SFTPConnection.makeJsonPayload(meta, true);
	}

	private static Payload makeJsonPayload(FileMetadata meta, boolean includeAll) {
		ObjectNode obj = JSONUtil.newObjectNode();
		obj.put(SFTPConstants.PROPERTY_FILENAME, meta.getFileName());
		obj.put(SFTPConstants.PROPERTY_REMOTE_DIRECTORY, meta.getParent());
		if (includeAll) {
			obj.put(SFTPConstants.MODIFIED_DATE, meta.getModifiedDateString());
			obj.put(SFTPConstants.FILESIZE, meta.getSize());
			obj.put(SFTPConstants.IS_DIRECTORY, meta.isDirectory());
		}
		return JsonPayloadUtil.toPayload((TreeNode) obj);
	}

	public String getHomeDirectory() {

		return client.getHomeDirectory();
	}

	public void reconnect() {
		closeConnection();
		this.openConnection();
	}

	public void uploadFile(String filePath, InputStream inputStream, long appendOffset) {
		long actualTransferredBytes;
		SFTPFileMetadata ftpFileMetadata = this.pathsHandler.splitIntoDirAndFileName(filePath);
		actualTransferredBytes = this.getSizeOnRemote(ftpFileMetadata.getDirectory(), ftpFileMetadata.getName());
		if (appendOffset > 0L) {
			actualTransferredBytes -= appendOffset;
		}

		SFTPConnection.adjustInputStreamToResumeUpload(inputStream, actualTransferredBytes);

		this.client.putFile(inputStream, filePath, ChannelSftp.APPEND);

	}

	private static void adjustInputStreamToResumeUpload(InputStream inputStream, long transferredBytes) {
		try {
			inputStream.reset();
			StreamUtil.skipFully(inputStream, transferredBytes);
		} catch (IOException e) {
			throw new ConnectorException(SFTPConstants.ERROR_RESUMING_UPLOAD, e);
		}
	}

	public long getSizeOnRemote(String directory, String name) {
		boolean filePresent = client.isFilePresent(pathsHandler.joinPaths(directory, name));
		if (filePresent) {
			return client.getFileMetadata(pathsHandler.joinPaths(directory, name)).getSize();
		} else {
			return 0L;
		}
	}

	public void getFile(String fileName, MeteredTempOutputStream outputStream) {
		long noOfBytestoskip = outputStream.getCount();
		this.client.retrieveFile(fileName, outputStream, noOfBytestoskip);

	}

	public SftpATTRS getSingleFileAttributes(String remoteDir, String fileName) {
		return client.getFileMetadata(pathsHandler.joinPaths(remoteDir, fileName));

	}

	public void deleteFile(String remoteDir, String fileName) {
		client.deleteFile(pathsHandler.joinPaths(remoteDir, fileName));

	}

	public List<LsEntry> getFileListInDirectory(String dirFullPath) {

		return client.getFileListInDirectory(dirFullPath);
	}

	public void testConnection() {
		openConnection();
		closeConnection();
	}
}

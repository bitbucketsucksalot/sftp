/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp.common;

import com.boomi.connector.api.JsonPayloadUtil;
import com.boomi.connector.api.Payload;
import com.boomi.connector.api.PayloadMetadata;
import com.boomi.connector.api.PayloadMetadataFactory;
import com.boomi.connector.sftp.constants.SFTPConstants;
import com.boomi.util.json.JSONUtil;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * @author Omesh Deoli
 *
 *         ${tags}
 */
public class SimpleSFTPFileMetadata implements SFTPFileMetadata {
	private final String remoteDir;
	private final String name;

	public SimpleSFTPFileMetadata(String remoteDir, String fileName) {
		this.remoteDir = remoteDir;
		this.name = fileName;
	}

	@Override
	public String getDirectory() {
		return this.remoteDir;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PayloadMetadata toPayloadMetadata(PayloadMetadataFactory payloadMetadataFactory) {
		PayloadMetadata metadata = payloadMetadataFactory.createMetadata();
		metadata.setTrackedProperty(SFTPConstants.PROPERTY_FILENAME, this.name);
		metadata.setTrackedProperty(SFTPConstants.REMOTE_DIRECTORY, this.remoteDir);
		return metadata;
	}

	@Override
	public Payload toJsonPayload() {
		return JsonPayloadUtil.toPayload((TreeNode) this.toJson());
	}

	protected ObjectNode toJson() {
		return JSONUtil.newObjectNode().put(SFTPConstants.PROPERTY_FILENAME, this.name)
				.put(SFTPConstants.REMOTE_DIRECTORY, this.remoteDir);
	}

}

/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp.common;

import com.boomi.connector.sftp.constants.SFTPConstants;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class SSHOptions {

	private String knownHostEntry;

	private Boolean sshkeyauth;

	private String sshkeypath;

	private String sshkeypassword;

	private Boolean dhKeySizeMax1024;

	public String getKnownHostEntry() {
		return knownHostEntry;
	}
	
	public SSHOptions(PropertiesUtil properties)
	{
		this.knownHostEntry=properties.getKnownHostEntry();
		this.sshkeyauth=properties.getAuthType().equals(SFTPConstants.USING_PUBLIC_KEY);
		this.sshkeypath=properties.getprvtkeyPath();
		this.sshkeypassword=properties.getpassphrase();
		this.dhKeySizeMax1024=properties.isMaxExchangeEnabled();
	}

	public void setKnownHostEntry(String knownHostEntry) {
		this.knownHostEntry = knownHostEntry;
	}

	public Boolean isSshkeyauth() {
		return sshkeyauth;
	}

	public void setSshkeyauth(Boolean sshkeyauth) {
		this.sshkeyauth = sshkeyauth;
	}

	public String getSshkeypath() {
		return sshkeypath;
	}

	public void setSshkeypath(String sshkeypath) {
		this.sshkeypath = sshkeypath;
	}

	public String getSshkeypassword() {
		return sshkeypassword;
	}

	public void setSshkeypassword(String sshkeypassword) {
		this.sshkeypassword = sshkeypassword;
	}

	public Boolean isDhKeySizeMax1024() {
		return dhKeySizeMax1024;
	}

	public void DhKeySizeMax1024(Boolean dhKeySizeMax1024) {
		this.dhKeySizeMax1024 = dhKeySizeMax1024;
	}
}

/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp;

import com.boomi.connector.api.OperationType;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public enum SFTPCustomType {
	LIST(OperationType.QUERY, SFTPObject.DIRECTORY), QUERY(OperationType.QUERY, SFTPObject.FILE);

	private final OperationType _superType;
	private final SFTPObject _object;

	private SFTPCustomType(OperationType superType, SFTPObject object) {
		this._superType = superType;
		this._object = object;
	}

	public OperationType getSuperType() {
		return this._superType;
	}

	public SFTPObject getObject() {
		return this._object;
	}
}

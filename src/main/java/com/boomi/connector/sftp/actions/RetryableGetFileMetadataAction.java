/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp.actions;

import com.boomi.connector.api.TrackedData;
import com.boomi.connector.sftp.SFTPConnection;
import com.jcraft.jsch.SftpATTRS;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class RetryableGetFileMetadataAction extends SingleRetryAction {
	private String fileName;
	private SftpATTRS fileMetaData;

	public void set_fileName(String _fileName) {
		this.fileName = _fileName;
	}

	public String get_fileName() {
		return fileName;
	}

	public SftpATTRS get_fileMetaData() {
		return fileMetaData;
	}

	public RetryableGetFileMetadataAction(SFTPConnection connection, String remoteDir, TrackedData input,
			String fileName) {
		super(connection, remoteDir, input);
		this.fileName = fileName;
	}

	public RetryableGetFileMetadataAction(SFTPConnection connection, String remoteDir, 
			String fileName) {
		super(connection, remoteDir, null);
		this.fileName = fileName;
	}
	public RetryableGetFileMetadataAction(SFTPConnection connection, String remoteDir, TrackedData input) {
		super(connection, remoteDir, input);

	}

	@Override
	public void doExecute() {
		this.fileMetaData = this.getConnection().getSingleFileAttributes(this.getRemoteDir(), fileName);

	}
}

/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp.actions;

import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.TrackedData;
import com.boomi.connector.sftp.SFTPConnection;
import com.boomi.connector.sftp.retry.RetryStrategyFactory;
import com.boomi.util.IOUtil;
import java.io.Closeable;
import java.io.InputStream;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class RetryableUploadFileAction extends RetryableAction implements Closeable {
	private final String filePath;
	private final InputStream inputStream;
	private final long appendOffset;

	public RetryableUploadFileAction(SFTPConnection connection, String remoteDir, RetryStrategyFactory retryFactory,
			String filePath, ObjectData input, long appendOffset) {
		super(connection, remoteDir, (TrackedData) input, retryFactory);
		this.filePath = filePath;
		this.inputStream = input.getData();
		this.appendOffset = appendOffset;
	}

	@Override
	public void doExecute() {
		this.getConnection().uploadFile(this.filePath, this.inputStream, this.appendOffset);
	}

	@Override
	public void close() {
		IOUtil.closeQuietly((Closeable[]) new Closeable[] { this.inputStream });
	}
}

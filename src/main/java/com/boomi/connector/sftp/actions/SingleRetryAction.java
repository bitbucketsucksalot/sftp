/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp.actions;

import com.boomi.connector.api.TrackedData;
import com.boomi.connector.sftp.SFTPConnection;
import com.boomi.connector.sftp.retry.RetryStrategyFactory;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
abstract class SingleRetryAction extends RetryableAction {
	private static final RetryStrategyFactory SINGLE_RETRY_FACTORY = RetryStrategyFactory.createFactory(1);

	SingleRetryAction(SFTPConnection connection, String remoteDir, TrackedData input) {
		super(connection, remoteDir, input, SINGLE_RETRY_FACTORY);
	}
}

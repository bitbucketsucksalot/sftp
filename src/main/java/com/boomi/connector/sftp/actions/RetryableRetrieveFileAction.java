/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp.actions;

import com.boomi.connector.api.TrackedData;
import com.boomi.connector.sftp.SFTPConnection;
import com.boomi.connector.sftp.common.MeteredTempOutputStream;
import com.boomi.connector.sftp.retry.RetryStrategyFactory;
import com.boomi.util.IOUtil;
import java.io.Closeable;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class RetryableRetrieveFileAction extends RetryableAction implements Closeable {
	private final String filePath;
	private final MeteredTempOutputStream outputStream = new MeteredTempOutputStream();

	public RetryableRetrieveFileAction(SFTPConnection connection, String remoteDir, String filePath, TrackedData input,
			RetryStrategyFactory retryFactory) {
		super(connection, remoteDir, input, retryFactory);
		this.filePath = filePath;
	}


	public MeteredTempOutputStream get_outputStream() {
		return outputStream;
	}

	@Override
	public void doExecute() {
		this.getConnection().getFile(this.filePath, this.outputStream);
	}

	@Override
	public void close() {
		IOUtil.closeQuietly((Closeable[]) new Closeable[] { this.outputStream });
	}
}

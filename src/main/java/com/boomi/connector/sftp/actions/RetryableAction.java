/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */

package com.boomi.connector.sftp.actions;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.TrackedData;
import com.boomi.connector.sftp.SFTPConnection;
import com.boomi.connector.sftp.exception.SFTPSdkException;
import com.boomi.connector.sftp.retry.RetryStrategyFactory;
import com.boomi.util.retry.PhasedRetry;
import com.boomi.util.retry.RetryStrategy;
import java.util.logging.Level;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public abstract class RetryableAction {
	private static final Object NULL_STATUS = null;
	private final SFTPConnection connection;
	private final String remoteDir;
	private final TrackedData input;
	private final RetryStrategyFactory retryFactory;

	RetryableAction(SFTPConnection connection, String remoteDir, TrackedData input, RetryStrategyFactory retryFactory) {
		this.connection = connection;
		this.remoteDir = remoteDir;
		this.input = input;
		this.retryFactory = retryFactory;
	}

	public TrackedData get_input() {
		return input;
	}

	public void execute() {
		RetryStrategy retry = this.retryFactory.createRetryStrategy();
		int numAttempts = 0;
		do {
			try {
				this.doExecute();
				return;
			} catch (SFTPSdkException e) {
				if (!retry.shouldRetry(++numAttempts, NULL_STATUS)) {
					throw new ConnectorException(e);
				}
				this.reconnect();

			}

		} while (true);
	}

	abstract void doExecute();

	private void reconnect() {
		PhasedRetry reconnectRetry = new PhasedRetry();
		int numAttempts = 0;
		do {
			try {
				this.getConnection().reconnect();
				return;
			} catch (ConnectorException e) {
				if (!reconnectRetry.shouldRetry(++numAttempts, NULL_STATUS)) {
					throw e;
				}
				this.input.getLogger().log(Level.WARNING, e.getMessage(), e);
				reconnectRetry.backoff(numAttempts);

			}

		} while (true);
	}

	SFTPConnection getConnection() {
		return this.connection;
	}

	String getRemoteDir() {
		return this.remoteDir;
	}
}

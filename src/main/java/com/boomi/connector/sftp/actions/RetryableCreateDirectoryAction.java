/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp.actions;

import com.boomi.connector.api.TrackedData;

import com.boomi.connector.sftp.SFTPConnection;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class RetryableCreateDirectoryAction extends SingleRetryAction {
	private String fullPath;
	
	public RetryableCreateDirectoryAction(SFTPConnection connection, String remoteDir, String fileName,
			TrackedData input) {
		super(connection, remoteDir, input);

	}

	public RetryableCreateDirectoryAction(SFTPConnection connection, String fullPath) {
		super(connection, null, null);
		this.fullPath = fullPath;
	}

	@Override
	public void doExecute() {
		this.getConnection().createNestedDirectories(fullPath);
	}

}

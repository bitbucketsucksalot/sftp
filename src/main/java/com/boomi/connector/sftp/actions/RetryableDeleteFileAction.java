/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp.actions;

import com.boomi.connector.api.ObjectIdData;
import com.boomi.connector.api.TrackedData;

import com.boomi.connector.sftp.SFTPConnection;
import com.boomi.connector.sftp.results.Result;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class RetryableDeleteFileAction extends SingleRetryAction {
	private ObjectIdData fileId;
	private Result result;

	public RetryableDeleteFileAction(SFTPConnection connection, String remoteDir, String fileName, TrackedData input) {
		super(connection, remoteDir, input);

	}

	public RetryableDeleteFileAction(SFTPConnection connection, ObjectIdData input) {
		super(connection, null, null);
		this.fileId = input;
	}

	@Override
	public void doExecute() {
		result = this.getConnection().deleteFile(fileId);
	}

	public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}
}

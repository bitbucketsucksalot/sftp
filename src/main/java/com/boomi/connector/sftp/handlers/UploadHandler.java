/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp.handlers;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.UUID;

import com.boomi.util.NumberUtil;
import com.boomi.util.ObjectUtil;
import com.boomi.util.StringUtil;
import com.boomi.util.json.JSONUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.api.TrackedData;

import com.boomi.connector.sftp.ActionIfFileExists;
import com.boomi.connector.sftp.SFTPConnection;
import com.boomi.connector.sftp.SFTPUtil;
import com.boomi.connector.sftp.actions.RetryableChangeDirectoryAction;
import com.boomi.connector.sftp.actions.RetryableDeleteFileAtPathAction;
import com.boomi.connector.sftp.actions.RetryableFindSizeOnRemote;
import com.boomi.connector.sftp.actions.RetryableFindUniqueFilenameAction;
import com.boomi.connector.sftp.actions.RetryableRenameFileAction;
import com.boomi.connector.sftp.actions.RetryableUploadFileAction;
import com.boomi.connector.sftp.actions.RetryableVerifyFileExistsAction;
import com.boomi.connector.sftp.common.ExtendedSFTPFileMetadata;
import com.boomi.connector.sftp.common.SFTPFileMetadata;
import com.boomi.connector.sftp.common.SimpleSFTPFileMetadata;
import com.boomi.connector.sftp.constants.SFTPConstants;
import com.boomi.connector.sftp.exception.SFTPSdkException;
import com.boomi.connector.sftp.retry.RetryStrategyFactory;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class UploadHandler extends BaseMultiInputHandler<ObjectData> {

	private final ActionIfFileExists actionIfFileExists;
	private final boolean includeAllMetadata;
	private final RetryStrategyFactory uploadRetryFactory;

	
	public UploadHandler(SFTPConnection connection, OperationResponse operationResponse) {
		super(connection, operationResponse);
		PropertyMap opProperties = this.connection.getContext().getOperationProperties();
		this.includeAllMetadata = UploadHandler.mustIncludeAllMetadata(this.connection);
		this.actionIfFileExists = NumberUtil.toEnum(ActionIfFileExists.class,
				(String) opProperties.getProperty(SFTPConstants.OPERATION_PROP_ACTION_IF_FILE_EXISTS), ActionIfFileExists.ERROR);
		this.uploadRetryFactory = RetryStrategyFactory.createFactory(12);

	}

	private UploadPaths getAndValidateNormalizedPaths(TrackedData input) {
		String enteredFilePath = SFTPUtil.getDocProperty(input, SFTPConstants.PROPERTY_FILENAME);
		if (StringUtil.isBlank(enteredFilePath)) {
			throw new SFTPSdkException(SFTPConstants.ERROR_MISSING_INPUT_FILENAME);
		}
		SFTPFileMetadata fileMetadata = this.extractRemoteDirAndFileName(input, enteredFilePath);
		String remoteDir = fileMetadata.getDirectory();
		String enteredFileName = fileMetadata.getName();
		String stagingDir = this.extractStagingDirectory(input, remoteDir);
		String finalFileName = this.extractFinalFileName(enteredFileName, remoteDir);
		String tempFileName = this.extractTempFileName(input, remoteDir, stagingDir, enteredFileName, finalFileName);
		return new UploadPaths(remoteDir, stagingDir, finalFileName, tempFileName);
	}

	private String extractStagingDirectory(TrackedData input, String remoteDir) {
		if (this.actionIfFileExists == ActionIfFileExists.APPEND) {
			return remoteDir;
		}
		String stagingDir = this.connection.getDocOrOperationProperty(input, SFTPConstants.PROPERTY_STAGING_DIRECTORY);
		if (StringUtil.isBlank( stagingDir)) {
			return remoteDir;
		}

		stagingDir = this.toFullPath(stagingDir);
		RetryableVerifyFileExistsAction fileExistsAction = new RetryableVerifyFileExistsAction(connection, stagingDir);
		fileExistsAction.execute();
		boolean fileExists = fileExistsAction.getFileExists();
		if (!ObjectUtil.equals( (stagingDir = this.toFullPath(stagingDir)), remoteDir)
				&& !ObjectUtil.equals( stagingDir,  this.defaultWorkingDirFullPath) && fileExists) {
			RetryableChangeDirectoryAction changeDirAcion = new RetryableChangeDirectoryAction(connection, stagingDir);
			changeDirAcion.execute();
		}
		if (!fileExists)
			throw new SFTPSdkException(SFTPConstants.ERROR_STAGING_DIR_NOT_FOUND);

		return stagingDir;
	}

	private String extractTempFileName(TrackedData input, String remoteDir, String stagingDir, String enteredFileName,
			String finalFileName) {
		if (this.actionIfFileExists == ActionIfFileExists.APPEND) {
			return finalFileName;
		}
		String tempExtension = this.connection.getDocOrOperationProperty(input, SFTPConstants.PROPERTY_TEMP_EXTENSION);
		boolean tempExtensionSpecified = StringUtil.isNotBlank( tempExtension);
		if (ObjectUtil.equals(remoteDir,stagingDir) && !tempExtensionSpecified && !actionIfFileExists.equals(ActionIfFileExists.OVERWRITE)) {
			return finalFileName;
		}
		tempExtension = tempExtensionSpecified
				? UploadHandler.assureExtensionStartsWithExtensionSeparator(tempExtension)
				: "";
		return enteredFileName + UUID.randomUUID() + tempExtension;
	}

	private static String assureExtensionStartsWithExtensionSeparator(String extension) {
		return StringUtil.startsWith(extension, SFTPConstants.EXTENSION_SEPARATOR) ? extension
				: SFTPConstants.EXTENSION_SEPARATOR + extension;
	}

	private static boolean mustRenameFile(String tempFileFullPath, String finalFileFullPath) {
		return !ObjectUtil.equals( tempFileFullPath,  finalFileFullPath);
	}

	private String extractFinalFileName(String enteredFileName, String remoteDir) {

		if (this.actionIfFileExists == ActionIfFileExists.FORCE_UNIQUE_NAMES) {
			RetryableFindUniqueFilenameAction uniqueFilenameAction = new RetryableFindUniqueFilenameAction(connection,
					enteredFileName, remoteDir);
			uniqueFilenameAction.execute();
			return uniqueFilenameAction.getUniqueFileName();

		}

		RetryableVerifyFileExistsAction fileExistsAction = new RetryableVerifyFileExistsAction(connection, remoteDir,
				enteredFileName);
		fileExistsAction.execute();

		if (

		fileExistsAction.getFileExists() && this.actionIfFileExists == ActionIfFileExists.ERROR) {
			String errorMessage = MessageFormat.format(SFTPConstants.ERROR_FILE_ALREADY_EXISTS_FORMAT, enteredFileName);
			throw new SFTPSdkException(errorMessage);
		}
		return enteredFileName;
	}

	private SFTPFileMetadata getSFTPFileMetadata(String remoteDir, String fileName) {
		if (!this.includeAllMetadata) {
			return new SimpleSFTPFileMetadata(remoteDir, fileName);
		}
		return this.getSFTPFileMetadataFromRemote(remoteDir, fileName);
	}

	private ExtendedSFTPFileMetadata getSFTPFileMetadataFromRemote(String remoteDir, String fileName) {
		return this.connection.getFileMetadata(remoteDir, fileName);
	}

	private static boolean mustIncludeAllMetadata(SFTPConnection connection) {
		JsonNode jsonCookie;
		String cookie = connection.getOperationContext().getObjectDefinitionCookie(ObjectDefinitionRole.OUTPUT);
		if (cookie == null) {
			throw new IllegalStateException(SFTPConstants.ERROR_INVALID_COOKIE);
		}
		try {
			jsonCookie = JSONUtil.getDefaultObjectMapper().readTree(cookie);
		} catch (IOException e) {
			throw new IllegalStateException(SFTPConstants.ERROR_INVALID_COOKIE, e);
		}
		return jsonCookie.path("includeAllMetadata").asBoolean();
	}

	private static final class UploadPaths {
		private final String remoteDirFullPath;
		private final String stagingDirFullPath;
		private final String finalFileName;
		private final String tempFileName;

		UploadPaths(String remoteDirFullPath, String stagingDirFullPath, String finalFileName, String tempFileName) {
			this.remoteDirFullPath = remoteDirFullPath;
			this.stagingDirFullPath = stagingDirFullPath;
			this.finalFileName = finalFileName;
			this.tempFileName = tempFileName;
		}

		String getRemoteDirFullPath() {
			return this.remoteDirFullPath;
		}

		String getStagingDirFullPath() {
			return this.stagingDirFullPath;
		}

		String getFinalFileName() {
			return this.finalFileName;
		}

		String getTempFileName() {
			return this.tempFileName;
		}
	}

	@Override
	void processInput(ObjectData input) {

		String finalFileName;
		String remoteDir;

		RetryableUploadFileAction uploadAction = null;
		try {
			UploadPaths uploadPaths = this.getAndValidateNormalizedPaths((TrackedData) input);
			finalFileName = uploadPaths.getFinalFileName();
			remoteDir = uploadPaths.getRemoteDirFullPath();
			String stagingDir = uploadPaths.getStagingDirFullPath();
			String tempFileName = uploadPaths.getTempFileName();
			String tempFileFullPath = this.pathsHandler.joinPaths(stagingDir, tempFileName);
			long appendOffset = 0L;
			if (actionIfFileExists.equals(ActionIfFileExists.APPEND)) {
				RetryableFindSizeOnRemote findSizeAction = new RetryableFindSizeOnRemote(connection, remoteDir,
						finalFileName);
				findSizeAction.execute();
				appendOffset = findSizeAction.getFileSize();
			}
			uploadAction = new RetryableUploadFileAction(this.connection, remoteDir, this.uploadRetryFactory,
					tempFileFullPath, input, appendOffset);
			uploadAction.execute();
			String finalFileFullPath = this.pathsHandler.joinPaths(remoteDir, finalFileName);
			if (UploadHandler.mustRenameFile(tempFileFullPath, finalFileFullPath)) {
				if (actionIfFileExists.equals(ActionIfFileExists.OVERWRITE)) {
					RetryableVerifyFileExistsAction fileExistsAction = new RetryableVerifyFileExistsAction(connection,
							finalFileFullPath);
					fileExistsAction.execute();
					if (fileExistsAction.getFileExists()) {
						RetryableDeleteFileAtPathAction retrydeleteAtPath = new RetryableDeleteFileAtPathAction(
								this.connection, finalFileFullPath);
						retrydeleteAtPath.execute();
					}
				}
				RetryableRenameFileAction retryRenameFile = new RetryableRenameFileAction(connection, tempFileFullPath,
						finalFileFullPath);
				retryRenameFile.execute();
			}
			SFTPFileMetadata fileMetadata = this.getSFTPFileMetadata(remoteDir, finalFileName);
			operationResponse.addResult((TrackedData) input, OperationStatus.SUCCESS, "0", SFTPConstants.FILE_CREATED,
					fileMetadata.toJsonPayload());
		} catch (Exception ex) {
			input.getLogger().info(ex.getMessage());
			throw ex;
		} finally {
			if (uploadAction != null) {
				uploadAction.close();
			}

		}
	}
}

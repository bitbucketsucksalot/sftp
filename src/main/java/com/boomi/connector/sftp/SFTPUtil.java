/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.TrackedData;
import com.boomi.connector.sftp.constants.SFTPConstants;
import com.boomi.util.StringUtil;
import java.text.MessageFormat;
import java.text.ParseException;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class SFTPUtil {
	private static final String TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
	private static final String ERROR_INVALID_TIMESTAMP = MessageFormat
			.format(SFTPConstants.ERROR_INVALID_TIMESTAMP_INPUT, TIMESTAMP_FORMAT);

	public static String getDocProperty(TrackedData input, String propName) {
		return StringUtil.trim((String) ((String) input.getDynamicProperties().get(propName)));
	}

	public static Date parseDate(String dateStr) {
		try {
			return SFTPUtil.getDateTimeFormat().parse(dateStr);
		} catch (ParseException e) {
			throw new ConnectorException(ERROR_INVALID_TIMESTAMP, (Throwable) e);
		}
	}

	public static Date parseDate(int dateInSeconds) {

		return new Date(dateInSeconds * 1000l);
	}

	public static String formatDate(Date date) {
		return SFTPUtil.getDateTimeFormat().format(date);
	}

	private static SimpleDateFormat getDateTimeFormat() {
		return new SimpleDateFormat(TIMESTAMP_FORMAT);
	}

}

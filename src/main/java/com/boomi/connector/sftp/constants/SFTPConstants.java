/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp.constants;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class SFTPConstants {
	public static final String PROPERTY_REMOTE_DIRECTORY = "directory";
	public static final String PROPERTY_MOVETO_DIRECTORY = "movetoDirectory";
	public static final String PROPERTY_FILENAME = "fileName";
	public static final String PROPERTY_PORT = "port";
	public static final String FILESIZE = "fileSize";
	public static final String IS_DIRECTORY = "isDirectory";
	public static final String MODIFIED_DATE = "modifiedDate";
	public static final String REMOTE_DIRECTORY = "remoteDirectory";

	public static final String PROPERTY_HOST = "host";
	public static final String PROPERTY_USERNAME = "username";
	public static final String PROPERTY_PASSWORD = "password";
	public static final String AUTHORIZATION_TYPE = "authType";

	public static final String INCLUDE_ALL="includeAll";
	public static final String APPEND="append";
	public static final String CREATE_DIR="createDir";
	public static final String USING_PUBLIC_KEY="Using public Key";
	
	
	public static final String OP_REGEX = "REGEX";
	public static final String OP_WILDCARD = "WILDCARD";
	public static final String UNKNOWN_EXPRESSION = "Unknown expression type: ";
	public static final String EXACTLY_ONE_ARGUEMENT_REQUIRED = "There should be exactly one argument. ''{0}'' given.";
	public static final String PATTERN_UNSUPPORTED_FOR_FILENAMES = "Pattern type operators (regex and wildcard) are currently only supported for filenames";
	public static final String UNKNOWN_PROPERTY = "Unknown property: ";
	public static final String QUOTE = "\"";
	public static final String INVALID_BOOLEAN_VALUE = "\" is not a valid boolean value";
	public static final String UNABLE_TO_PARSE_DATE = "Unable to parse date: \"";
	public static final String REGEX = "regex";
	public static final String GLOB = "glob";
	public static final String MATCHING_WITH_PATTERN = "Matching \"%s\" with pattern \"%s\" (op = %s); result = %b";
	public static final String COMPARISON_YIELDS = "Comparison %s %s %s yields %b";
	public static final String UNKNOWN_GROUPING_OPERATOR = "Unknown grouping operator: ";
	public static final String PATH_TYPE = "FilePath";
	public static final String COMPARABLE_TYPE = "Comparable";
	public static final String BOOLEAN_TYPE = "Boolean";
	public static final String OBJECT_TYPE_FILE = "File";
	public static final String SIMPLE_FILE_META_SCHEMA_PATH = "/schemas/simple-file-metadata.schema.json";
	public static final String EXTENDED_FILE_META_SCHEMA_PATH = "/schemas/extended-file-metadata.schema.json";
	public static final String ERROR_SCHEMA_LOAD_FORMAT = "the '%s' schema could not be loaded.";
	public static final String PROPERTY_INCLUDE_METADATA = "includeAllMetadata";

	public static final String SFTP = "sftp";
	public static final String ERROR_FAILED_RENAME = "Could not rename path ''{0}'' to ''{1}''";
	public static final String ERROR_FAILED_FILE_RETRIEVAL = "An error occurred while retrieving the file at path ''{0}''";
	public static final String ERROR_FAILED_DIRECTORY_CREATE = "An error occurred while creating the ''{0}'' directory.";
	public static final String ERROR_FAILED_LISTING_FILENAMES = "An error occured while listing the files in directory";
	public static final String ERROR_FAILED_FILE_REMOVAL = "An error occurred while removing the file at path ''{0}''";
	public static final String ERROR_FAILED_RETRIEVING_DIRECTORY = "An error occured while listing the files in directory";
	public static final String ERROR_FAILED_CHANGING_CURRENT_DIRECTORY = "An error occured while changing the current directory to ''{0}'' ";
	public static final String ERROR_FAILED_FILE_UPLOAD = "An error occurred while creating the file at path ''{0}''";
	public static final String ERROR_FAILED_CONNECTION_TO_HOST = "Sftp connection to host failed";
	public static final String ERROR_FAILED_CREATING_SESSION = "An error occurred while creating session";
	public static final String ERROR_FAILED_CONNECTION_TO_PORT = "An error occurred while connecting to port";
	public static final String ERROR_FAILED_SFTP_LOGIN = "Sftp login failed";
	public static final String ERROR_FAILED_SFTP_SERVER_CONNECTION = "Error connecting to SFTP server";
	public static final String ERROR_FAILED_OPENING_SFTP_CHANNEL = "Error opening SFTP channel";
	public static final String ERROR_FAILED_GETING_FILE_METADATA = "Error getting file metadata at path ''{0}''";
	public static final String ERROR_FAILED_GETING_DIRECTORY_CONTENTS = "Error getting directory contents at path ''{0}''";
	public static final String ERROR_FAILED_CHECKING_FILE_EXISTENCE = "Error checking file existence at path ''{0}''";
	public static final String LEGACY_ALGO_LIST = "diffie-hellman-group1-sha1,diffie-hellman-group14-sha1,diffie-hellman-group-exchange-sha1";
	public static final String KEX = "kex";
	public static final String PROP_STRICT_HOST_KEY_CHECKING = "StrictHostKeyChecking";
	public static final String SHKC_NO = "no";
	public static final String AUTH_SEQUENCE_FULL = "publickey,password,keyboard-interactive";
	public static final String PREFERRED_AUTHENTICATIONS = "PreferredAuthentications";
	public static final String KEY_COMP_S2C_ALG = "compression.s2c";
	public static final String KEY_COMP_C2S_ALG = "compression.c2s";
	public static final String DH_GROUP_EXCHANGE_SHA1 = "diffie-hellman-group-exchange-sha1";
	public static final String DH_GROUP_EXCHANGE_SHA256 = "diffie-hellman-group-exchange-sha256";
	public static final String CLASS_DHGEX1024 = "com.boomi.connector.sftp.DHGEX1024";
	public static final String CLASS_DHGEX256_1024 = "com.boomi.connector.sftp.DHGEX256_1024";
	public static final String COMP_ALGS = "zlib,none";
	public static final String SIGNATURE_DSS_KEY = "signature.dss";
	public static final String SIGNATURE_RSA_KEY = "signature.rsa";
	public static final String CAUSE = " Cause: ";
	public static final String UNABLE_TO_PARSE_SSH_KEY = "Failed to load or parse SSH Key.  ";
	public static final String UTF_8 = "UTF-8";
	public static final String UNABLE_TO_PPROCESS_KNOWN_HOST_KEY = "Unable to process SSH Known Hosts Entry. Exception message is: ";
	public static final String DISCONNECTING_FROM_SFTP_SERVER = "Disconnecting from SFTP server.";
	public static final String ERROR_DISCONNECTING_CHANNEL = "Errors occurred disconnecting channel, will ignore.";
	public static final String ERROR_DISCONNECTING_SESSION = "Errors occurred disconnecting session, will ignore.";
	public static final String FILE_NOT_FOUND = "File Not Found";
	public static final String ERROR_GETTING_HOME_DIR = "Error fetching home directory";
	public static final String YES = "yes";
	public static final String ERROR_MISSING_INPUT_FILENAME = "the 'File Name' input document property is required, and must contain a non-blank value. This property contains the name of the file to upload.";
	public static final String ERROR_MISSING_DIRECTORY = "A directory must be specified in the connection settings or as a document property for this operation";
	public static final String ERROR_QUERYING_FILES = "Unexpected error while querying files";
	public static final String LIMIT_MUST_BE_POSITIVE = "Limit must be positive or -1 (given: %d)";
	public static final String UNEXPECTED_ERROR_OCCURED = "An unexpected error occurred. File: \"%s\"";
	public static final String COUNT = "count";
	public static final String ERROR_RESUMING_UPLOAD = "Error resuming upload";
	public static final String ERROR_CREATING_JSON_DEFINITION = "Error creating JSON definition";
	public static final String UNKNOWN_DEFINITION_ROLE = "Unknown definition role: ";
	public static final String ERROR_REMOTE_DIRECTORY_NOT_FOUND = "Remote directory does not exist";
	public static final String PROPERTY_DELETE_AFTER = "deleteAfter";
	public static final String PROPERTY_FAIL_DELETE_AFTER = "failDeleteAfter";
	public static final String ERROR_MISSING_INPUT_FILENAME_ID = "ID must contain a non-blank value. Id is the name of the file to download.";
	public static final String ERROR_UNABLE_TO_DELETE_FILE = "Unable to delete file";
	public static final String PROPERTY_STAGING_DIRECTORY = "stagingDirectory";
	public static final String PROPERTY_TEMP_EXTENSION = "tempExtension";
	public static final String OPERATION_PROP_ACTION_IF_FILE_EXISTS = "actionIfFileExists";
	public static final String ERROR_FILE_ALREADY_EXISTS_FORMAT = "the ''{0}'' file already exists in the remote directory on the SFTP server.";
	public static final String ERROR_INVALID_COOKIE = "the cookie containing the browsing metadata for Create is not set, or does not contain a JSON value.";
	public static final String EXTENSION_SEPARATOR = ".";
	public static final String ERROR_STAGING_DIR_NOT_FOUND = "Staging directory does not exist";
	public static final String FILE_CREATED ="Created the file successfully";
	public static final String ERROR_DELETING_FILE ="There was an unexpected error deleting \"%s\"";
	public static final String INVALID_QUERY_TYPE ="Invalid Query type: ";
	public static final String ERROR_SEARCHING ="An unexpected error occurred while searching";
	
	public static final String SUCCESS_MESSAGE = "Success";
	public static final String FAILURE_CODE = "-1";
	public static final String SUCCESS_CODE = "0";
	public static final String FILE_NOT_FOUND_CODE = "1";
	
	public static final String ERROR_LIST = "errors";

	public static final String MESSAGE = "message";
	public static final String CODE = "code";
	public static final String ERROR_OCCURED_IN_FILES ="An error occurred in %d files";
	public static final String ERROR_IN_ERR_DOC ="Error occurred while finishing error document";
	public static final String KEY_PSWRD="keyPswrd";
	public static final String KEY_PATH="keyPath";
	public static final String HOST_ENTRY="hostEntry";
	public static final String IS_MAX_EXCHANGE="isMaxExchange";
	public static final String INVALID_PATH="Path must be non-null";
	public static final String ERROR_PARSING_DATE_FROM_FILESYSTEM="Unable to parse date returned from file system";
	public static final String ERROR_INVALID_TIMESTAMP_INPUT="The specified timestamp is invalid and must be provided in this format ({0}).";
	public static final String UNABLE_TO_DELETE_FILE="Unable to delete the file";
	public static final String PRIVATE_KEY_CONTENT="prvkeyContent";
	public static final String PUBLIC_KEY_CONTENT = "pubkeyContent";
	public static final String KEY_PAIR_NAME ="keyPairName";
	public static final String USE_KEY_CONTENT = "useKeyContent";
	
	
	

}

/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */

package com.boomi.connector.sftp;

import java.nio.charset.StandardCharsets;

/**
  * @author Omesh Deoli
  *
  * ${tags}
  */
public class PublicKeyParam {

	private String passphrase;
	private String prvkeyPath;
	private String user;

	private byte[] prvkeyContent;
	private byte[] pubkeyContent;
	private byte[] passphraseContent;
	
	private String keyPairName;
	private boolean useKeyContent;

	public byte[] getPassphraseContent() {
		return passphraseContent;
	}

	public void setPassphraseContent(byte[] passphraseContent) {
		this.passphraseContent = passphraseContent;
	}

	public PublicKeyParam(String passphrase, String prvkeyPath, String user, boolean useKeyContent) {
		super();
		this.passphrase = passphrase;
		this.prvkeyPath = prvkeyPath;
		this.user = user;
		this.useKeyContent = useKeyContent;
	}

	public PublicKeyParam(String user,String passphrase, String prvkeyContent, String pubkeyContent, String keyPairName,
			boolean useKeyContent) {
		super();
		this.passphraseContent = passphrase.getBytes(StandardCharsets.UTF_8);
		this.prvkeyContent = formatPrivateKey(prvkeyContent).getBytes(StandardCharsets.UTF_8);
		this.pubkeyContent = formatPublicKey(pubkeyContent).getBytes(StandardCharsets.UTF_8);
		this.keyPairName = keyPairName;
		this.useKeyContent = useKeyContent;
		this.user=user;	
	}

	public boolean isUseKeyContentEnabled() {
		return useKeyContent;
	}

	public void setUseKeyContent(boolean useKeyContent) {
		this.useKeyContent = useKeyContent;
	}

	public byte[] getPrvkeyContent() {
		return prvkeyContent;
	}

	public void setPrvkeyContent(byte[] prvkeyContent) {
		this.prvkeyContent = prvkeyContent;
	}

	public byte[] getPubkeyContent() {
		return pubkeyContent;
	}

	public void setPubkeyContent(byte[] pubkeyContent) {
		this.pubkeyContent = pubkeyContent;
	}

	public String getKeyPairName() {
		return keyPairName;
	}

	public void setKeyPairName(String keyPairName) {
		this.keyPairName = keyPairName;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassphrase() {
		return passphrase;
	}

	public void setPassphrase(String passphrase) {
		this.passphrase = passphrase;
	}

	public String getPrvkeyPath() {
		return prvkeyPath;
	}

	public void setPrvkeyPath(String prvkeyPath) {
		this.prvkeyPath = prvkeyPath;
	}
	
	/**
	 * Formatting the private id_rsa key.
	 * @param prvkey
	 * @return
	 */
	public String formatPrivateKey(String prvkey) {
		
		String[] prvtkeySplit = prvkey.split(" ");
		StringBuilder bld = new StringBuilder();

		for (int i = 0; i < prvtkeySplit.length; i++) {
			if (i == prvtkeySplit.length - 1) {
				bld.append(prvtkeySplit[i]);
			} else {
				if (prvtkeySplit[i].contains("BEGIN") || prvtkeySplit[i].contains("RSA")
						|| prvtkeySplit[i].contains("PRIVATE") || prvtkeySplit[i].contains("END")
						|| prvtkeySplit[i].endsWith(":")) {
					bld.append(prvtkeySplit[i]).append(" ");
				} else {
					bld.append(prvtkeySplit[i]).append("\n");
				}
			}
		}
		return bld.toString();
	}

	/**
	 * Formatting the public id_rsa key.
	 * @param publicKey
	 * @return
	 */
	public String formatPublicKey(String publicKey) {

		if(!publicKey.endsWith("\n")) {
			return publicKey.concat("\n");
		}
		return publicKey;
	}
	
}

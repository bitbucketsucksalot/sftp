/*******************************************************************************
 * /*
 * *  Copyright 2019 Accenture. All Rights Reserved.
 * *  The trademarks used in these materials are the properties of their respective owners.
 * *  This work is protected by copyright law and contains valuable trade secrets and
 * *  confidential information.
 * */
package com.boomi.connector.sftp.operations;

import com.boomi.connector.api.DeleteRequest;
import com.boomi.connector.api.ObjectIdData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.TrackedData;
import com.boomi.connector.sftp.SFTPConnection;
import com.boomi.connector.sftp.actions.RetryableDeleteFileAction;
import com.boomi.connector.sftp.constants.SFTPConstants;
import com.boomi.connector.sftp.exception.NoSuchFileFoundException;
import com.boomi.connector.sftp.results.EmptySuccess;
import com.boomi.connector.sftp.results.ErrorResult;
import com.boomi.connector.sftp.results.Result;
import com.boomi.connector.util.BaseConnection;
import com.boomi.connector.util.BaseDeleteOperation;
import com.boomi.util.LogUtil;
import java.util.logging.Logger;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class SFTPDeleteOperation extends BaseDeleteOperation {
	public SFTPDeleteOperation(SFTPConnection conn) {
		super((BaseConnection) conn);
	}

	protected void executeDelete(DeleteRequest deleteRequest, OperationResponse operationResponse) {
		SFTPConnection conn = this.getConnection();
		try {
		conn.openConnection();
		for (ObjectIdData input : deleteRequest) {
			Result result;
			try {
				RetryableDeleteFileAction retryableDelete = new RetryableDeleteFileAction(conn, input);
				retryableDelete.execute();
				result = retryableDelete.getResult();
				result.addToResponse(operationResponse, (TrackedData) input);
			} catch (NoSuchFileFoundException e) {
				result = new EmptySuccess();
				result.addToResponse(operationResponse, (TrackedData) input);
			} catch (Exception e) {
				LogUtil.severe((Logger) input.getLogger(), e,
						 SFTPConstants.ERROR_DELETING_FILE,
						(Object[]) new Object[] { input.getObjectId() });
				result = new ErrorResult(e);
				result.addToResponse(operationResponse, (TrackedData) input);
			}			
		}
		}
		catch(Exception e) {
			ResponseUtil.addExceptionFailures(operationResponse, (Iterable) deleteRequest, (Throwable) e);
		}
		finally {
			conn.closeConnection();
		}
	}

	public SFTPConnection getConnection() {
		return (SFTPConnection) super.getConnection();
	}
}
